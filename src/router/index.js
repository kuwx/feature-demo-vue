import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
  path: '/index',
  name: 'index',
  component: () => import('../views/index/index.vue')
  },
  {
    path: '/demo1',
    name: 'demo1',
    component: () => import('../views/demo1/index.vue')
  },
  {
    path: '/demo2',
    name: 'demo2',
    component: () => import('../views/demo2/index.vue')
  },
  {
  path: '/',
  name: 'index',
  component: () => import('../views/index/index.vue')
  },
]
const router = new VueRouter({
  mode: "hash",
  routes
})

export default router